

exports.setup = function (req, res) {

	req.getConnection(function (err, connection) {
		var query = connection.query("\
    CREATE TABLE IF NOT EXISTS variable (\
      name varchar(128) NOT NULL DEFAULT '' COMMENT 'The name of the variable.',\
      value varchar(2048) NOT NULL COMMENT 'The value of the variable.',\
      PRIMARY KEY (name)\
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Named variable/value pairs';\
    ", [], function (err, rows)
		{
			if (err)
				console.log("Error Selecting : %s ", err);

			connection.query("INSERT INTO variable (name, value) VALUES(?, ?)", ['raspberrypi_ip', ''], function (error, resp) {
				if (err) {
					console.log("Error Selecting : %s ", error);
					return next(error);
				}
				res.render('settings/setup', {page_title: "Setup Database", data: rows[0]});
			});
			console.log(query.sql);
		});

	});
};

exports.set_raspberrypi_ip = function (req, res) {
	var input = JSON.parse(JSON.stringify(req.body));
	req.getConnection(function (err, connection) {
		var name = "raspberrypi_ip";
		var data = {
			value: input.ip
		};
		connection.query("UPDATE variable set ? WHERE name=? ", [data, name], function (err, rows)
		{
			if (err)
				console.log("Error Updating : %s ", err);
			console.log(rows);
            res.send(rows);
			// res.redirect('/settings');
		});
	});
};

exports.get_raspberrypi_ip = function (req, res) {
	req.getConnection(function (err, connection) {
		connection.query('SELECT value from variable WHERE name=?', ['raspberrypi_ip'], function (err, results) {
			if (!!results && results.length) {
				var ip = results[0].value;
				res.send({
					'ip': ip
				});
			}
		});
	});
};
