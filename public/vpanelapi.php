<?php
namespace vk\iot;

class MyCurl {

    private $service_url;

    public function __construct($service_url) {
        $this->service_url = $service_url;
    }

    private function init($action) {
        $curl = curl_init($this->service_url . $action);
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        return $curl;
    }

    private function process($curl) {
        $curl_response = curl_exec($curl);
        if ($curl_response !== false) {
            curl_close($curl);
            $response = json_decode($curl_response);
            return $response;
        } else {
            $info = curl_getinfo($curl);
            curl_close($curl);
            throw new \Exception('Curl error: ' . serialize($info));
        }
    }

    public function get($action) {
        $curl = $this->init($action);
        return $this->process($curl);
    }

    public function post($action, $data) {
        $curl = $this->init($action);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        return $this->process($curl);
    }
}

class VpanelApi {

    private $curl;
    const SERVICE_URL = "http://localhost:1337/";
//    const SERVICE_URL = "http://vpanel.azurewebsites.net/";

    public function __construct(){
        $this->curl = new \vk\iot\MyCurl(self::SERVICE_URL);
    }

    public function getIP() {
        return $this->curl->get("settings/raspberrypi_ip");
    }

    public function setIP($ip) {
        return $this->curl->post("settings/raspberrypi_ip", array('ip' => $ip));
    }

}

use vk\iot as api;
$api = new api\VpanelApi();

$update = $api->setIP("192.168.1.115");
var_dump($update);

$ip = $api->getIP();
var_dump($ip);
