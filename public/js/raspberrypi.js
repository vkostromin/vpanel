panel.controller('raspberrypi', function($scope, $http) {


    $scope.update = function () {
      console.log($scope.new_ip);
        $http.post('/settings/raspberrypi_ip', {'ip': $scope.new_ip}).then(function(result){
            return $scope.get();
        });
    };

    $scope.get = function () {
        $http.get('/settings/raspberrypi_ip').then(function(result){
            $scope.ip = result.data.ip;
            return result.data.ip;
        });
    };
    $scope.get();
});
