var express = require('express')
  , xml2js = require('xml2js')
  , request = require('request')
;
var bodyParser = require('body-parser');

var app = express();
//var routes = require('./routes');
var path = require('path');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(express.static('public'));

var mysql = require('mysql');
var connection = require('express-myconnection');

var settings = require('./routes/settings.js');

//mysql -hus-cdbr-azure-west-c.cloudapp.net -ub495b5c009edc5 -p8bb786c7 vpanelapi
app.use(
  connection(mysql, {
      host: 'us-cdbr-azure-west-c.cloudapp.net',
//      host: 'localhost',
      user: 'b495b5c009edc5',
//      user: 'api',
      password: '8bb786c7',
      port: 3306, //port mysql
      database: 'vpanelapi',
      debug: true,
          multipleStatements: true
		}, 'single')
  );

app.get('/seasonvar', function(req, res) {
    request('http://seasonvar.ru/rss.php', function (error, response, body) {
        if (error) return;
        var parser = new xml2js.Parser();
        parser.parseString(body, function (err, result) {
            res.send(result.rss.channel[0].item);
        });
    });
});

app.get('/settings/setup', settings.setup);//html
app.get('/settings/raspberrypi_ip', settings.get_raspberrypi_ip);//json
app.post('/settings/raspberrypi_ip', settings.set_raspberrypi_ip);//json

var server = app.listen(process.env.port || 1337, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Example app listening at http://%s:%s', host, port);
});
